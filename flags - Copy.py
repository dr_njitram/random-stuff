import matplotlib
from codecs import open
from os import listdir
import os  
import pandas as pd
from scipy.cluster.vq import whiten
from scipy.cluster.vq import kmeans

def get_distance(a, b):
    distance = 0
    for no, elem in enumerate(a):
        if no > 2:
            continue
        distance += (elem - b[no])**2
    return distance

def get_main_color(image_name):
    print(image_name)
    if 'BYB' in image_name:
        return "194 88 209", "194 88 209"
    image = matplotlib.image.imread(r'C:\Users\Martijn\Documents\Paradox Interactive\Imperator\mod\moreflags\gfx\coat_of_arms\textured_emblems\\' + image_name)
    r = []
    g = []
    b = []
    for line in image:
        for pixel in line:
            if len(pixel) == 3:
                temp_r, temp_g, temp_b = pixel
            else:
                temp_r, temp_g, temp_b, temp_a = pixel
            r.append(temp_r)
            g.append(temp_g)
            b.append(temp_b)
    
    df = pd.DataFrame({'red': r,'blue': b,'green': g})
        
    df['scaled_red'] = whiten(df['red'])
    df['scaled_blue'] = whiten(df['blue'])
    df['scaled_green'] = whiten(df['green'])
    
    cluster_centers, distortion = kmeans(df[['scaled_red', 'scaled_green', 'scaled_blue']], 3)
    
    colors = []
    r_std, g_std, b_std = df[['red', 'green', 'blue']].std()
    for cluster_center in cluster_centers:
        scaled_r, scaled_g, scaled_b = cluster_center
        colors.append((int(scaled_r * r_std * 255),int(scaled_g * g_std * 255),int(scaled_b * b_std * 255)))
        
    
    initial_color = [int(_) for _ in image[0][0]*255]
    
    distances = [get_distance(initial_color, sub_color) for sub_color in colors]
    
    primary = colors.pop(distances.index(min(distances)))
    secondary = colors.pop()
    
    primary = " ".join([str(_) for _ in primary])
    secondary = " ".join([str(_) for _ in secondary])
    return primary, secondary



mod_flags = r'C:\Users\Martijn\Documents\Paradox Interactive\Imperator\mod\moreflags\gfx\coat_of_arms\textured_emblems'
mod_path = r'C:\Users\Martijn\Documents\Paradox Interactive\Imperator\mod\morecolors\common'
countries_file = r'C:\Program Files (x86)\Steam\steamapps\common\ImperatorRome\game\common\countries.txt'
base_path = r'C:\Program Files (x86)\Steam\steamapps\common\ImperatorRome\game\common\\'

tags = {}

for file in listdir(mod_flags):
    tag = file.split('.')[0]
    if len(tag) == 3:
        tags[tag] = file
    if 'exception' in tag:
        tags[file[:3]] = file

tags_keys = list(tags.keys())

countries_lines = open(countries_file, 'r', 'utf-8-sig').readlines()

for line in countries_lines:
    if line.startswith('#') or line.strip() == '':
                       continue
    tag = line.split('=')[0].strip()
    
    if tag in tags:
        files = line.split('=')[1].strip()[1:-1].replace('/', '\\')
        paths = mod_path + '\\' + files
        paths = "\\".join(paths.split('\\')[:-1])
    
        print(paths)
        if os.path.isdir(paths) is False:
            os.makedirs(paths)
        exit(-1)
        read_lines = open(base_path + files, 'r' ,'utf-8-sig').readlines() 
        mod_file = open(mod_path + '\\' + files, 'w' ,'utf-8-sig')
        colors = get_main_color(tags[tag])
        for line in read_lines:
            if 'color1' in line or 'color' in line:
                mod_file.write("color1 = rgb {" + colors[0] + "}\n")
            elif 'color2' in line:
                mod_file.write("color2 = rgb {" + colors[1] + "}\n")
            else:
                mod_file.write(line)
