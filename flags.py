import matplotlib
from codecs import open
from os import listdir
import pandas as pd
from scipy.cluster.vq import whiten
from scipy.cluster.vq import kmeans

def get_distance(a, b):
    distance = 0
    for no, elem in enumerate(a):
        if no > 2:
            continue
        distance += (elem - b[no])**2
    return distance

def get_main_color(image_name):
    print(image_name)
    if 'BYB' in image_name:
        return "194 88 209", "194 88 209"
    image = matplotlib.image.imread(r'C:\Users\Martijn\Documents\Paradox Interactive\Imperator\mod\moreflags\gfx\coat_of_arms\textured_emblems\\' + image_name)
    r = []
    g = []
    b = []
    for line in image:
        for pixel in line:
            if len(pixel) == 3:
                temp_r, temp_g, temp_b = pixel
            else:
                temp_r, temp_g, temp_b, temp_a = pixel
            r.append(temp_r)
            g.append(temp_g)
            b.append(temp_b)
    
    df = pd.DataFrame({'red': r,'blue': b,'green': g})
        
    df['scaled_red'] = whiten(df['red'])
    df['scaled_blue'] = whiten(df['blue'])
    df['scaled_green'] = whiten(df['green'])
    
    cluster_centers, distortion = kmeans(df[['scaled_red', 'scaled_green', 'scaled_blue']], 3)
    
    colors = []
    r_std, g_std, b_std = df[['red', 'green', 'blue']].std()
    for cluster_center in cluster_centers:
        scaled_r, scaled_g, scaled_b = cluster_center
        colors.append((int(scaled_r * r_std * 255),int(scaled_g * g_std * 255),int(scaled_b * b_std * 255)))
        
    
    initial_color = [int(_) for _ in image[0][0]*255]
    
    distances = [get_distance(initial_color, sub_color) for sub_color in colors]
    
    primary = colors.pop(distances.index(min(distances)))
    secondary = colors.pop()
    
    primary = " ".join([str(_) for _ in primary])
    secondary = " ".join([str(_) for _ in secondary])
    return primary, secondary



mod_flags = r'C:\Users\Martijn\Documents\Paradox Interactive\Imperator\mod\moreflags\gfx\coat_of_arms\textured_emblems'

tags = {}

for file in listdir(mod_flags):
    tag = file.split('.')[0]
    if len(tag) == 3:
        tags[tag] = file
    if 'exception' in tag:
        tags[file[:3]] = file

tags_keys = list(tags.keys())

new_file = open(r'C:\Users\Martijn\Documents\Paradox Interactive\Imperator\mod\moreflags\common\coat_of_arms\coat_of_arms\01_new_flags.txt', 'w', 'utf-8')
new_file.truncate()

template = "%(TAG)s = {\n\tcolor1 = rgb { %(color1)s }\n\tcolor2 = rgb { %(color2)s }\n\ttextured_emblem = {\n\t\ttexture = \"%(filename)s\"\n\t}\n}\n"

for tag in tags:
    colors = get_main_color(tags[tag])
    formatted_string = template % {"TAG": tag, "color1": colors[0], "color2": colors[1], "filename": tags[tag] }
    new_file.write(formatted_string)



